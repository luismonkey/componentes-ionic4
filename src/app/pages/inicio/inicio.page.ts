import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

	componentes: Componente[] = [
	{
		icon: 'add-circle-outline',
		name: 'Action Sheet',
		redirectTo: '/action-sheet'
	},
	{
		icon: 'appstore',
		name: 'Alert',
		redirectTo: '/alert'
	},
	{
		icon: 'help-circle-outline',
		name: 'About',
		redirectTo: '/about'
	},
	{
		icon: 'person',
		name: 'Avatar',
		redirectTo: '/avatar'
	},
	{
		icon: 'radio-button-on',
		name: 'Botones',
		redirectTo: '/buttons'
	},
	{
		icon: 'card',
		name: 'Cards',
		redirectTo: '/card'
	},
	{
		icon: 'checkmark-circle-outline',
		name: 'Checkbox',
		redirectTo: '/check'
	},
	{
		icon: 'calendar',
		name: 'DateTime',
		redirectTo: '/date-time'
	},
	{
		icon: 'share',
		name: 'Fab',
		redirectTo: '/fab'
	},
	{
		icon: 'grid',
		name: 'Grid',
		redirectTo: '/grid'
	},
	{
		icon: 'infinite',
		name: 'Infinite Scroll',
		redirectTo: '/infinite-scroll'
	},
	{
		icon: 'hammer',
		name: 'Input',
		redirectTo: '/input'
	},
	{
		icon: 'list',
		name: 'Listas',
		redirectTo: '/list'
	}
	];

  constructor() { }

  ngOnInit() {
  }

}

interface Componente {
	icon: string;
	name: string;
	redirectTo: string;
}
